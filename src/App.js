import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import GreetingCat from './GreetingCat';
import RandomCat from './RandomCat';
import './App.css';

const defaultHistory = createBrowserHistory();

function App({ history = defaultHistory }) {
	return (
		<Router history={history}>
			<Routes>
				<Route exact path='/cats/' element={<RandomCat />} />
				<Route exact path='/cats/cat/:greeting' element={<GreetingCat />} />
			</Routes>
		</Router>
	);
}

export default App;
